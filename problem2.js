const fs = require("fs");
const path = require('path');

function callback(cb) {
    if (typeof cb !== "function") {
        console.error("passed callback is not a function");
    } else {
        try {
            let defaultDir = path.dirname("./data/lipsum.txt");
            let defaultPath = path.join(defaultDir, 'lipsum.txt');
            let fileNamesPath = path.join(defaultDir, 'fileNames.txt');
            let fileNamesString = ''
            fs.readFile(defaultPath, "utf-8", (err, defaultData) => {

                if (err) {
                    cb(err);
                } else {
                    let upperCaseFileName = 'upperCase.txt';
                    let upperCaseFilePath = path.join(defaultDir, upperCaseFileName);
                    fs.writeFile(upperCaseFilePath, defaultData.toUpperCase(), (err) => {

                            if(err) {
                                cb(err);
                            } else {
                                fileNamesString += upperCaseFileName;
                                fs.writeFile(fileNamesPath, fileNamesString, (err) => {

                                    if(err) {
                                        cb(err);
                                    } else {
                                        fs.readFile(upperCaseFilePath, "utf-8", (err, upperCaseFileData) => {

                                            if(err) {
                                                cb(err);
                                            } else {
                                                let lowerCaseFileName = 'lowerCase.txt';
                                                let lowerCaseFilePath = path.join(defaultDir, lowerCaseFileName);
                                                fs.writeFile(lowerCaseFilePath, upperCaseFileData.toLowerCase(), (err) => {

                                                    if(err) {
                                                        cb(err);
                                                    } else {
                                                        fileNamesString += ' ' + lowerCaseFileName;
                                                        fs.writeFile(fileNamesPath, fileNamesString, (err) => {

                                                            if(err) {
                                                                cb(err);
                                                            } else {
                                                                fs.readFile(lowerCaseFilePath, "utf-8", (err, lowerCaseFileData) => {

                                                                    if(err) {
                                                                        cb(err);
                                                                    } else {
                                                                        let sortedFileName = 'sortedData.txt';
                                                                        let sortedFilePath = path.join(defaultDir, sortedFileName);
                                                                        let sortedData = lowerCaseFileData.toString().split('\n').sort((a,b) => a-b).toString();
                                                                        fs.writeFile(sortedFilePath, sortedData, (err) => {

                                                                            if(err) {
                                                                                cb(err);
                                                                            } else {
                                                                                fileNamesString += ' ' + sortedFileName;
                                                                                fs.writeFile(fileNamesPath, fileNamesString, (err) => {

                                                                                    if(err) {
                                                                                        cb(err);
                                                                                    } else {
                                                                                        fs.readFile(fileNamesPath, "utf-8", (err, fileNamesData) => {

                                                                                            if(err) {
                                                                                                cb(err);
                                                                                            } else {
                                                                                                fileNamesData.split(' ').forEach((element) => {
                                                                                                    
                                                                                                    setTimeout(() => {
                                                                                                        let filePath = path.join(defaultDir, element);
                                                                                                        fs.unlink(filePath, (err) => {

                                                                                                            if(err) {
                                                                                                                cb(err);
                                                                                                            }
                                                                                                        });
                                                                                                    }, 2000);
                                                                                                });
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        } 
                    );
                }
            });
        } catch (error) {
            cb(error);
        }
    }
}

module.exports = callback;

/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
