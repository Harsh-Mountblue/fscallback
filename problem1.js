const fs = require("fs");
const path = require('path');

function callback(cb) {
    if (typeof cb !== "function") {
        console.error("passed callback is not a function");
    } else {
        try {
            const folderName = path.dirname('./') + "/tempData";

            fs.exists(folderName, (exist) => {
                if (exist) {
                    createFiles();
                } else {
                    fs.mkdir(folderName, () => {
                        createFiles();
                    });
                }
            });

            function createFiles() {
                let numOfFiles = Math.ceil(Math.random() * 10);
                let filePaths = [];
                let errors = [];
                let writeCount = 0;
                let deleteCount = 0;

                for (let index = 0; index < numOfFiles; index++) {
                    let tempName = randomString(Math.ceil(Math.random() * 10));
                    let jsonPath = path.join(folderName, tempName) + ".JSON"; 

                    filePaths.push(jsonPath);

                    fs.writeFile(
                        jsonPath,
                        JSON.stringify(tempName.split()),
                        (err) => {
                            if (err) {
                                errors.push(err);
                            }
                            writeCount += 1;
                            if (writeCount === numOfFiles) {
                                if (errors.length === numOfFiles) {
                                    cb(errors);
                                } else {
                                    filePaths.forEach((file) => {
                                        setTimeout(() => {
                                            fs.unlink(file, (err) => {
                                                if (err) {
                                                    errors.push(err);
                                                }
                                                deleteCount += 1;
                                                if (deleteCount === numOfFiles) {
                                                    if(errors.length > 0) {
                                                        cb(errors);
                                                    }
                                                }
                                            });
                                        }, 2000);
                                    });
                                }
                            }
                        }
                    );
                }
            }
        } catch (err) {
            cb(err);
        }
    }
    function randomString(charLength) {
        let randomStr = "";

        for (let index = 0; index < charLength; index++) {
            let randomNum = Math.ceil(Math.random() * 26);
            let tempRandom = Math.floor(Math.random() * 2);
            randomStr += tempRandom
                ? String.fromCharCode(randomNum + 64)
                : String.fromCharCode(randomNum + 96);
        }
        return randomStr;
    }
}

module.exports = callback;

/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
